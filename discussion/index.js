console.log("Hello");
/* ES6 Updates
 */

//Exponent Operator:

const firstNum = Math.pow(8,2)
console.log(firstNum);

//updates is Exponent Operator we use "**"

const secondNum = 8 ** 2
console.log(secondNum);

//5 raised to the power of 5
const thirdNum = 5**5
console.log(thirdNum);

// Template Literals
console.log("");
// allows us to write strings w/o concatination operator (+)

let name = "George"

console.log("Hello your name is: " + name);

// Concatination / Pre-Template Literal 
//Using single qoute ''

//let message = 

//String using Template Literal
//Uses the backticks (` `)
let message = `Hello ${name}. Welcome to Zuitt Bootcamp`
console.log(`Message with template literal: ${message}`);
console.log("");

let anotherMessage = `${name} attended a math competition. 
He won it by solving the problem 8**2 with solution of ${secondNum}
`

console.log(anotherMessage);


//another message
anotherMessage = "\n " + name + ' attended a math competition. \n He won it by solving the problem 8**2 with the solution ' + secondNum + '. \n'

console.log(anotherMessage)

//Operation inside Template literal

console.log("");

const interestRate = .1
const principalValue = 1000

console.log(`The Interest on your savings ${principalValue * interestRate}`);


// Destructuring of Arrays
/* 
            Array destructing allows to unpack elements in an array
            into distinct variables. Allows us to name the array elements with variables 
            instead of index number.

            Suntax:
            let/const [variableName, variableName, variableName ] = array;
*/
console.log("");
const fullName = ["Joe","Dela","Cruz"]
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

//template literal
console.log(`Hello, ${fullName[0]}  ${fullName[1]}  ${fullName[2]} ! It's nice to meet you.`);

//Array Destructuring
console.log("");
const [firstName, middleName, lastName] = fullName ;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello, ${firstName}  ${middleName}  ${lastName} ! It's nice to meet you.`);

// Object Destructuring
/* Allows to unpack properties of objects 
into distinct variable. Shortends the Syntax for 
accessing properties from objects */

/* Synxtax:
        let/const {propertyName,propertyName,propertyName} = objectName*/

        
        console.log("")
        const person = {
            givenName: "Jane",
            maidenName: "Dela",
            familyName: "Cruz"
        }
        
        // Pre-Object Destructuring
        console.log(person.givenName)
        console.log(person.maidenName)
        console.log(person.familyName)
        
        function getFullName(givenName, maindenName, familyName) {
            console.log(`${givenName} ${maindenName} ${familyName}`)
        }
        
        getFullName(person.givenName, person.maidenName, person.familyName)
        
        
        // Using Object Destructuring
        const {maidenName, familyName, givenName} = person
        console.log(givenName)
        console.log(maidenName)
        console.log(familyName)
        
        function getFullName(given, maiden, family) {
            console.log(`${given} ${maiden} ${family}`)
        }
        
        getFullName(givenName, maidenName, familyName)
        
        

//Mini Activity:
//part 1
let employess = ['Mary Jane', 'Lonzo Boi', 'Lebron JamesReid'];

console.log("");
const [employeeName1, employeeName2, employeeName3] = employess ;
console.log(employeeName1);
console.log(employeeName2);
console.log(employeeName3);

console.log(`Hello Employees , ${employeeName1}  ${employeeName2}  ${employeeName3} 
! It's nice to meet you.`);


//part2
console.log("");
let pets = {
        dogName: "Brownie",
        catName: "Kuting",
        fishName: "LonzoBoy"

}
const {dogName, catName, fishName} = pets
console.log(dogName)
console.log(catName)
console.log(fishName)

function getPetName(pet1, pet2, pet3) {
    console.log(`${pet1} ${pet2} ${pet3}`)
}

getPetName(catName, dogName, fishName)

// PRE-ARROW FUNCTION AND ARROW FUNCTION

/* 
Syntax:
    function functionName(paramA, paramB){
        statement/return/console.log
    } */

    function printFullName(fName, mName, lName) {
        console.log(fName+" "+mName+" "+lName);
    }
    printFullName('Emman', 'D','Garcia')


    //ArrowFunc

    /* 
    Syntax:
    let/const variableNmae = (paramA, paramB) => {
        statement/console/return
    } */

    const printFullName1 = (fName , mName , lName) => {
        console.log(`${fName} ${mName} ${lName}`);
    }
    printFullName1('Derrick' , 'Martell', 'Rose')


    const students = ['Yoby', 'Emman','Ronel']
    //Functions Loop
    //Pre Arrow Func

    // students.forEach(function(student)){
    //     console.log(student + " is a student");
    // }

    // students.forEach


    //Implicit Return Statement
//pre arrow
    console.log("");
    function add(x,y){
        return x+y
    }
    let total = add(12, 15);
    console.log(total);

    //arrow func
    const addition = (x,y) => x+y
    /* 
            const addition = (x,y) => {
                return x+y
            }

            only use return keyword when curly braces {} is used
    
    */

    let total2 = addition(12,15)
    console.log(total2);


    //Default Function Argument Value:
    console.log("");

    const greet = (name = "User") => {
        return `Good evening, ${name}`
    }
    console.log(greet());
    console.log(greet("Archie"));

    //Class-Based Object Blueprint
    /* 
    Allows creation/instantiation of objects 
    using class as Blueprint
        Syntax:
        class classsName {
            constructor(objectPropertyA,objectPropertyB ){
                this.objectPropertyA = objectPropertyA
                this.objectPropertyB = objectPropertyB
            }
       
    
    */

    console.log("");

class Car{
        constructor(brand, name , year){
            this.brand = brand
            this.name = name
            this.year = year
        }
}
const myCar = new Car()
console.log(myCar);

myCar.brand = "Ford"
myCar.name = "Raptor"
myCar.year = 2021
console.log(myCar);

const myNewCar = new Car("Toyota", "Vios",2019)
console.log(myNewCar);