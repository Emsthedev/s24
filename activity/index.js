// S24 Activity Template:
	/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:
    console.log("Hello");

    const Num = 2 ** 3
    console.log(`The cube of 2 is ${Num}`);

	/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


	// Code here:
    const fullAddress = ["258 Wasingtion Ave NW","California","90011"]


//Array Destructuring

const [street, city, zip] = fullAddress ;
console.log(`I live at ${street},  ${city}  ${zip} `);

	/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:
    const animal = {
        animalName: "Lolong",
        animalWeight: 1075,
        animalHeight: "20 ft 3 in"
    }
    const {animalName, animalHeight, animalWeight} = animal
  
    function getFullName(name, height, weight) {
        console.log(`${name} was a salt water crocodile. He weighed at ${weight} kgs with a measurement of ${height}`)
    }
    
    getFullName(animalName, animalHeight, animalWeight)


	/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

	// Code here:
const number = [1,2,3,4,5,15];
const disp =
number.forEach((num) => {
	console.log(`${num}`)
})
console.log(disp);


/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

	// Code here:
    console.log("");
    class Dog{
        constructor(name2, age , breed){
            this.name2 = name2
            this.age = age
            this.breed = breed
        }
}

const myDog = new Dog("Frankie", 5,'miniature Dachschund')
console.log(myDog);